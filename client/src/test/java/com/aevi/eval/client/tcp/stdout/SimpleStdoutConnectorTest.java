package com.aevi.eval.client.tcp.stdout;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockserver.netty.MockServer;

import java.net.ConnectException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.Assertions.fail;

class SimpleStdoutConnectorTest {

    MockServer mockServer;

    @AfterEach
    void tearDown() {
        mockServer.stop();
    }

    @Test
    void shouldNotThrowAnyException_whileConnectionAttempt_withUpMockRemoteServer_soThatConnectionEstablished() {
        try {
            mockServer = new MockServer(8080);
            new SimpleStdoutConnector("localhost", 8080).connect();
        } catch (Exception e) {
            fail(
                    "Something went wrong during the connection with a mock remote server. " +
                            "Probably, the server is shutdown.",
                    e
            );
        }
    }

    @Test
    void shouldThrowConnectException_whileConnectionAttempt_withDownMockRemoteServer_soThatConnectionRefused() {
        assertThat(
                catchThrowableOfType(
                        () -> {
                            mockServer = new MockServer(8080);
                            new SimpleStdoutConnector("localhost", 8180).connect();
                        },
                        ConnectException.class
                )
        )
                .isNotNull();

    }

}