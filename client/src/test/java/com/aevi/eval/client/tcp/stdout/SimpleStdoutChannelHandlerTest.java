package com.aevi.eval.client.tcp.stdout;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class SimpleStdoutChannelHandlerTest {

    @Test
    void shouldWriteOutboundMessage_uponSuccessfulConnection_withServer() {
        assertThat(new EmbeddedChannel(new SimpleStdoutChannelHandler()).outboundMessages())
                .containsExactly("[Remote-Client]: Connected.\n");
    }

    @Test
    void shouldReceiveInboundMessage_fromConnectedChannel_thatIsServer() {
        final ArgumentCaptor<String> inboundMessageCaptor = ArgumentCaptor.forClass(String.class);
        final String outMessageExpected = "Test";
        PrintStream out = mock(PrintStream.class);
        SimpleStdoutChannelHandler handler = spy(new SimpleStdoutChannelHandler());
        System.setOut(out);

        new EmbeddedChannel(handler).writeInbound(outMessageExpected);
        verify(handler, times(1)).channelRead0(any(ChannelHandlerContext.class), inboundMessageCaptor.capture());
        assertThat(outMessageExpected).isEqualTo(inboundMessageCaptor.getValue());
        verify(out, times(1)).println(inboundMessageCaptor.capture());
        assertThat(outMessageExpected).isEqualTo(inboundMessageCaptor.getValue());
    }

}