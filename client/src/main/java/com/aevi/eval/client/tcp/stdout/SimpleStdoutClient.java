package com.aevi.eval.client.tcp.stdout;

import java.util.Scanner;

public class SimpleStdoutClient {

    public static final String LOCAL_HOST = "localhost";
    public static final String DEFAULT_REMOTE_HOST = LOCAL_HOST;
    public static final int DEFAULT_REMOTE_PORT = 8080;

    private final String host;
    private final int port;

    public SimpleStdoutClient(int port) {
        this(DEFAULT_REMOTE_HOST, port);
    }

    public SimpleStdoutClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        new SimpleStdoutConnector(host, port)
                .connect()
                .listen(channel -> {
                    while (scanner.hasNext()) {
                        String input = scanner.nextLine();
                        if (input.contains("-exit")) {
                            channel.write("[Remote-Stdout-Client]: Disconnecting...\n");
                            channel.write("-exit\n");
                            channel.flush();
                            break;
                        }
                        channel.writeAndFlush("[Remote-Stdout-Client]: " + input + '\n');
                    }
                });
    }

}
