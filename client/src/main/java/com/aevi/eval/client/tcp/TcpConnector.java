package com.aevi.eval.client.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

public abstract class TcpConnector {

    private final SocketAddress remoteAddress;

    public TcpConnector(String host, int port) {
        remoteAddress = new InetSocketAddress(host, port);
    }

    protected abstract ChannelInitializer<SocketChannel> getHandler();

    public TcpConnection connect() {
        try {
            EventLoopGroup group = new NioEventLoopGroup();
            return new TcpConnection(
                    group,
                    new Bootstrap()
                            .group(group)
                            .channel(NioSocketChannel.class)
                            .handler(getHandler())
                            .connect(remoteAddress)
                            .sync()
                            .channel()
            );
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
