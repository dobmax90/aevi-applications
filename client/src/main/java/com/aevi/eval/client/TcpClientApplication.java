package com.aevi.eval.client;

import com.aevi.eval.client.tcp.stdout.SimpleStdoutClient;

import static com.aevi.eval.client.tcp.stdout.SimpleStdoutClient.DEFAULT_REMOTE_HOST;
import static com.aevi.eval.client.tcp.stdout.SimpleStdoutClient.DEFAULT_REMOTE_PORT;

public class TcpClientApplication {
    public static void main(String[] args) {
        String host = args.length > 0 ? args[0] : DEFAULT_REMOTE_HOST;
        int port = args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_REMOTE_PORT;
        new SimpleStdoutClient(host, port).start();
    }
}
