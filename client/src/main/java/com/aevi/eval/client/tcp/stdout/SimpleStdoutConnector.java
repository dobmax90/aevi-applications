package com.aevi.eval.client.tcp.stdout;

import com.aevi.eval.client.tcp.TcpConnector;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

class SimpleStdoutConnector extends TcpConnector {

    public SimpleStdoutConnector(String host, int port) {
        super(host, port);
    }

    @Override
    protected ChannelInitializer<SocketChannel> getHandler() {
        return new SimpleStdoutChannelInitializer();
    }

}
