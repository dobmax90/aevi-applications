package com.aevi.eval.client.tcp;

import io.netty.channel.Channel;

@FunctionalInterface
public interface ChannelWriterHandler {
    void write(Channel channel);
}
