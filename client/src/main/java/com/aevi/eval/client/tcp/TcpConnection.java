package com.aevi.eval.client.tcp;

import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;

public class TcpConnection {

    private final EventLoopGroup group;
    private final Channel channel;

    public TcpConnection(EventLoopGroup group, Channel channel) {
        this.group = group;
        this.channel = channel;
    }

    public void listen(ChannelWriterHandler handler) {
        try {
            handler.write(channel);
            channel.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }
}
