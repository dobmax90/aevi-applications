package com.aevi.eval.server.tcp;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

public abstract class TalkativeTcpServer extends TcpServer {

    public TalkativeTcpServer(InetSocketAddress inetSocketAddress) {
        super(inetSocketAddress);
    }

    protected abstract OutboundMessageHandler getOutboundMessageHandler();

    @Override
    protected void start(ServerBootstrap bootstrap, SocketAddress address) {
        try {
            ChannelFuture channelFuture = bootstrap.bind(address).sync();

            getOutboundMessageHandler().handle();

            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            throw new TcpServerException("Something went wrong during TCP Server processing ...", e);
        }
    }

}
