package com.aevi.eval.server.tcp;

public class TcpServerException extends RuntimeException {
    public TcpServerException(String message) {
        super(message);
    }

    public TcpServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
