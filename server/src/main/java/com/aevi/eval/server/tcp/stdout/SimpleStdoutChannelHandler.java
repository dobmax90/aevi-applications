package com.aevi.eval.server.tcp.stdout;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;

class SimpleStdoutChannelHandler extends SimpleChannelInboundHandler<String> {

    private final ChannelGroup activeChannels;

    SimpleStdoutChannelHandler(ChannelGroup activeChannels) {
        this.activeChannels = activeChannels;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        activeChannels.add(ctx.channel());
        ctx.write("INFO: Connected successfully.\n");
        ctx.write("INFO: The conversation started.\n");
        ctx.write("INFO: Exit command is `-exit`\n");
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        if ("-exit".equals(msg)) {
            ChannelFuture channelFuture = ctx.writeAndFlush("INFO: Disconnected successfully.\nINFO: The conversation ended.\n");
            channelFuture.addListener(ChannelFutureListener.CLOSE);
        }

        System.out.println(msg);
    }

}
