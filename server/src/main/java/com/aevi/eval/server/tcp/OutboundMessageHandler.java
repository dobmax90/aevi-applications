package com.aevi.eval.server.tcp;

@FunctionalInterface
public interface OutboundMessageHandler {
    void handle();
}
