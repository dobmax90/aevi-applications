package com.aevi.eval.server.tcp.stdout;

import com.aevi.eval.server.tcp.OutboundMessageHandler;
import com.aevi.eval.server.tcp.TalkativeTcpServer;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.InetSocketAddress;
import java.util.Scanner;

public class SimpleStdoutServer extends TalkativeTcpServer {

    public static final int DEFAULT_TCP_PORT = 8080;
    private final ChannelGroup channelGroup;
    private final int port;

    public SimpleStdoutServer(int port) {
        super(new InetSocketAddress(port));
        this.port = port;
        channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    }

    @Override
    protected ChannelInitializer<SocketChannel> getChildHandler() {
        return new SimpleStdoutHandlerInitializer(channelGroup);
    }

    @Override
    protected OutboundMessageHandler getOutboundMessageHandler() {
        return () -> {
            System.out.println("INFO: Application successfully started on port: " + port);
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                String input = scanner.nextLine();
                channelGroup.writeAndFlush("[Server-User]: " + input + "\n");
            }
        };
    }

}
