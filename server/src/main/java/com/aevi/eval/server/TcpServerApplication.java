package com.aevi.eval.server;

import com.aevi.eval.server.tcp.stdout.SimpleStdoutServer;

import static com.aevi.eval.server.tcp.stdout.SimpleStdoutServer.DEFAULT_TCP_PORT;

public class TcpServerApplication {
    public static void main(String[] args) {
        int port = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_TCP_PORT;
        new SimpleStdoutServer(port).start();
    }
}
