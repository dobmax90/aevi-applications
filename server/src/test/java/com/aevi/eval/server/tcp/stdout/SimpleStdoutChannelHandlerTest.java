package com.aevi.eval.server.tcp.stdout;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SimpleStdoutChannelHandlerTest {

    @Test
    void shouldAddMessages_toOutboundQueue_uponNewChannelActivation() {
        assertThat(
                new EmbeddedChannel(new SimpleStdoutChannelHandler(mock(ChannelGroup.class)))
                        .outboundMessages()
        )
                .containsExactly(
                        "INFO: Connected successfully.\n",
                        "INFO: The conversation started.\n",
                        "INFO: Exit command is `-exit`\n"
                );
    }

    @Test
    void shouldAddMessages_toOutboundQueue_uponExitOperationFromExistingChannel() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new SimpleStdoutChannelHandler(mock(ChannelGroup.class)));
        embeddedChannel.releaseOutbound();
        embeddedChannel.writeInbound("-exit");
        assertThat(embeddedChannel.outboundMessages())
                .containsExactly("INFO: Disconnected successfully.\nINFO: The conversation ended.\n");
    }

    @Test
    void shouldDisconnectExistingChannel_uponInboundExitCommand() {
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new SimpleStdoutChannelHandler(mock(ChannelGroup.class)));
        embeddedChannel.releaseOutbound();
        embeddedChannel.writeInbound("-exit");
        assertThat(embeddedChannel.isActive()).isFalse();
    }

    @Test
    void shouldRegisterNewChannel_intoChannelGroup_uponSuccessfulActivation() {
        DefaultChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
        SimpleStdoutChannelHandler handler = new SimpleStdoutChannelHandler(channelGroup);

        ChannelHandlerContext handlerContextMock = mock(ChannelHandlerContext.class);
        when(handlerContextMock.channel()).thenReturn(new EmbeddedChannel());

        handler.channelActive(handlerContextMock);
        assertThat(channelGroup.size()).isOne();
    }

    @Test
    void shouldUnregisterExistingChannel_fromChannelGroup_uponExitCommand() {
        ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
        SimpleStdoutChannelHandler handler = new SimpleStdoutChannelHandler(channelGroup);
        EmbeddedChannel channel = new EmbeddedChannel(handler);
        ChannelHandlerContext context = channel.pipeline().context(SimpleStdoutChannelHandler.class);

        assertThat(channelGroup.size()).isOne();
        handler.channelRead0(context, "-exit");
        assertThat(channelGroup.size()).isZero();
    }

}