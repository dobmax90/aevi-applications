# AEVI Eval applications

Create 2 simple TCP (not HTTP) client and server applications that talk to each other by simply sending messages. 
The user interface is arbitrary, even the command line is fine. Write at least one unit test + instructions on how to use the application.

## Installation & User Guides

### Server
#### Build requirements
- Java 17
- Maven 3.6.3
#### Create an artifact
For a build procedure Maven is used. Make sure Maven is installed on a build machine.

Step to be done for a build:
- navigate to a source folder via executing the command `cd /path/to/your/sources/server`
- execute the command `mvn clean package`
- check the folder created `/path/to/your/sources/server/target`
- check the artifact created `/path/to/your/sources/server/target/sso-server-eavi.jar`
#### Run requirements
- Java 17
#### Run an application
A built executable jar is working application and does not need any specific configuration.

Basic command for the application startup: `java -jar /path/to/jar/location/sso-server-eavi.jar`

**Note:** Since the application is working over TCP-protocol, up on startup a default port will be occupied.

A default port is `8080`. Please make sure a default port is free, beforehand.

It is possible to set a custom port up, in case a default port is occupied or just on-demand.

Command for the application run with a custom port: `java -jar /path/to/your/jar/location/sso-server-eavi.jar 8180`

### Client
#### Build requirements
- Java 17
- Maven 3.6.3
#### Create an artifact
For a build procedure Maven is used. Make sure Maven is installed on a build machine.

Step to be done for a build:
- navigate to a source folder via executing the command `cd /path/to/your/sources/client`
- execute the command `mvn clean package`
- check the folder created `/path/to/your/sources/client/target`
- check the artifact created `/path/to/your/sources/client/target/sso-client-eavi.jar`
#### Run requirements
- Java 17
#### Run an application
A built executable jar is working application and does not need any specific configuration.

Basic command for the application startup: `java -jar /path/to/jar/location/sso-client-eavi.jar`

A default remote host is `localhost`.

A default remote port is `8080`. Please make sure a default port is free, beforehand.

It is possible to set up a custom remote host and port.

**Note:** Make sure that program arguments should be in a strict order like:
- First argument is a remote host
- Second argument is a remote port

So that, next command samples will occur the issue while staring the application up.
- `java -jar /path/to/your/jar/location/sso-client-eavi.jar 8180`
- `java -jar /path/to/your/jar/location/sso-client-eavi.jar 8180 localhost`

Valid command samples for setting a custom remote host and\or port
- `java -jar /path/to/your/jar/location/sso-client-eavi.jar 127.0.0.2`
- `java -jar /path/to/your/jar/location/sso-client-eavi.jar localhost 8180`
- `java -jar /path/to/your/jar/location/sso-client-eavi.jar 127.0.0.2 8180`